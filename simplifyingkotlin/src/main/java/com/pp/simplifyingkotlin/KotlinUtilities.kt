package com.pp.simplifyingkotlin

import android.content.Intent
import android.view.View
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

inline fun <T, R> Collection<T>.mapIf(transformation: (T) -> R, condition: (T) -> Boolean): Collection<R> = this.filter { condition(it) }.map { transformation(it) }

fun FragmentManager.doTransaction(block: FragmentTransaction.() -> Unit) {
    val transaction = beginTransaction()
    block(transaction)
    transaction.commit()
}

fun <T> FragmentActivity.goTo(destination: Class<T>, extras: (R: Intent) -> Unit = {}) {
    startActivity(Intent(this, destination).apply(extras))
}

fun <T> FragmentActivity.goAndReturn(code: Int, destination: Class<T>, extras: (R: Intent) -> Unit = {}) {
    startActivityForResult(Intent(this, destination).apply(extras), code)
}

fun View.onClick(block: (View) -> Unit) {
    setOnClickListener {
        block(it)
    }
}