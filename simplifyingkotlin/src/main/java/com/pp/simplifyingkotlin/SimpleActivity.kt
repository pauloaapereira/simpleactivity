package com.pp.simplifyingkotlin

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.AnimBuilder
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions

@Suppress("UNCHECKED_CAST")
abstract class SimpleActivity(
    @LayoutRes private val layoutId: Int,
    private val orientation: Int = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT,
    private val canGoToPreviousActivity: Boolean = false
) : AppCompatActivity(), ActivityType, NavigationBehaviours,
    ViewModelBehaviours, MenuBehaviours,
    LifecycleBehaviours, LifecycleObserver {

    private lateinit var viewModel: ViewModel

    override var actionBarTitle: String
        get() = supportActionBar?.title?.toString() ?: ""
        set(value) {
            supportActionBar?.title = value
        }

    override var customToolbar: View?
        get() = supportActionBar?.customView
        set(value) {
            supportActionBar?.customView = value
        }

    @IdRes
    override var navHost: Fragment? = null
    override val currentFrag: Fragment?
        get() = navHost?.childFragmentManager?.fragments?.get(0)

    override fun doOnBackPressed() {}
    override fun doOnOptionsItemSelected(item: MenuItem?) {}

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    override fun doOnCreate() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun doOnResume() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun doOnStop() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    override fun doOnPause() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun doOnDestroy() {}

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    override fun doOnStart() {}

    override fun <T : ViewModel> setViewModel(viewModelClass: Class<T>) {
        viewModel = ViewModelProviders.of(this).get(viewModelClass)
    }

    override fun <T : ViewModel> viewModel(viewModelClass: Class<T>) = viewModel as T


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = orientation
        setContentView(layoutId)
        lifecycle.addObserver(this)
        supportActionBar?.setDisplayHomeAsUpEnabled(canGoToPreviousActivity)
    }

    override fun onBackPressed() {
        doOnBackPressed()
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
        }
        item?.let {
            doOnOptionsItemSelected(item)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun navigate(@IdRes fragId: Int, args: Bundle?, options: NavOptions?) {
        navHost?.findNavController()?.navigate(fragId, args, options)
    }

    override fun navAnimOptions(animBuilder: AnimBuilder.() -> Unit) =
        navOptions {
            anim {
                animBuilder()
            }
        }

}

interface ActivityType {
    var actionBarTitle: String
    var customToolbar: View?
}

interface NavigationBehaviours {
    var navHost: Fragment?
    val currentFrag: Fragment?
    fun navigate(@IdRes fragId: Int, args: Bundle? = null, options: NavOptions? = null)
    fun navAnimOptions(animBuilder: AnimBuilder.() -> Unit): NavOptions
}

interface LifecycleBehaviours {
    fun doOnCreate()
    fun doOnResume()
    fun doOnStart()
    fun doOnStop()
    fun doOnPause()
    fun doOnDestroy()
}

interface MenuBehaviours {
    fun doOnBackPressed()
    fun doOnOptionsItemSelected(item: MenuItem?)
}

interface ViewModelBehaviours {
    fun <T : ViewModel> setViewModel(viewModelClass: Class<T>)
    fun <T : ViewModel> viewModel(viewModelClass: Class<T>): T
}